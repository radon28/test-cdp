<?php
namespace App\Traits;

use Illuminate\Support\Facades\Storage;
/**
 *
 */
trait FileUpload
{
  public function fileUpload($file)
  {
      return Storage::put("public", $file);
  }
}
?>