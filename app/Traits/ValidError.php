<?php
namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
/**
 *
 */
trait ValidError
{
  function failedValidation(Validator $validator)
  {
    $errors = (new ValidationException($validator))->errors();
     throw new HttpResponseException(
         response()->json(['message'=>'Validation failed', 'errors' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
     );
  }
}
