<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface AdminRepositoryInterface
{
   public function all(): Collection;
}