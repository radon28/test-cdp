<?php 
namespace App\Repository\Eloquent;

use App\Models\Product;
use App\Repository\ProductRepositoryInterface;
use Illuminate\Support\Collection;
use App\Traits\FileUpload;

class ProductRepository extends EloquentRepository implements ProductRepositoryInterface
{
    use FileUpload;
    const LIMIT = 5;
   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
   public function __construct(Product $model)
   {
       parent::__construct($model);
   }

   /**
    * @return Collection
    */
   public function all(): Collection
   {
       return $this->model->all();    
   }

   public function addProduct(array $attributes)
   {
        $attributes['image'] = $this->fileUpload($attributes['image']);
        $product=$this->create($attributes);
        return ['name'=>$product->name];
   }

   public function editProduct($product, array $attributes)
   {
        if (isset($attributes['image']) && $attributes['image'])
            $attributes['image'] = $this->fileUpload($attributes['image']);
        $product->update($attributes);
        return ['name'=>$product->name];
   }

   public function getActiveProducts($limit = self::LIMIT)
   {
       return $this->model->where('status', 1)->paginate($limit);
   }
}