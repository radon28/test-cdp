<?php 
namespace App\Repository\Eloquent;

use App\Models\User;
use App\Repository\UserRepositoryInterface;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use Auth;

class UserRepository extends EloquentRepository implements UserRepositoryInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
   public function __construct(User $model)
   {
       parent::__construct($model);
   }

   /**
    * @return Collection
    */
   public function all(): Collection
   {
       return $this->model->all();    
   }

   public function createUser(array $attributes)
   {
        $attributes['password']=bcrypt($attributes['password']);
        $user=$this->create($attributes);
        return ['name'=>$user->first_name." ".$user->last_name,'email'=>$user->email];
   }

   public function login(array $attributes)
   {
        if(auth()->guard('user')->attempt($attributes)) {
            $token =  auth()->guard('user')->user()->createToken('MyApp',['user']);
            return response()->json([
                "message"=>"login successful",
                "data"=>[
                    'access_token' => $token->accessToken,
                    'token_type' => 'Bearer',
                    'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString()
                ]
            ], 200);
        }else{ 
            return response()->json(['error' => ['Email and Password are Wrong.']], 200);
        }
   }
}