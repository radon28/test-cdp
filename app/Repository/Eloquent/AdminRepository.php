<?php 
namespace App\Repository\Eloquent;

use App\Models\Admin;
use App\Repository\AdminRepositoryInterface;
use Illuminate\Support\Collection;
use Carbon\Carbon;

class AdminRepository extends EloquentRepository implements AdminRepositoryInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
   public function __construct(Admin $model)
   {
       parent::__construct($model);
   }

   /**
    * @return Collection
    */
   public function all(): Collection
   {
       return $this->model->all();    
   }

   public function createUser(array $attributes)
   {
        $attributes['password']=bcrypt($attributes['password']);
        $user=$this->create($attributes);
        return ['name'=>$user->name,'email'=>$user->email];
   }

   public function login(array $attributes)
   {
        if(auth()->guard('admin')->attempt($attributes)) {
            $token =  auth()->guard('admin')->user()->createToken('MyApp',['admin']);
            return response()->json([
                "message"=>"login successful",
                "data"=>[
                    'access_token' => $token->accessToken,
                    'token_type' => 'Bearer',
                    'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString()
                ]
            ], 200);
        }else{ 
            return response()->json(['error' => ['Email and Password are Wrong.']], 200);
        }
    }
}