<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

     /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if($exception instanceof \Illuminate\Auth\AuthenticationException) {
            return response()->json(['message'=>'Unauthenticated', 'type'=>'auth','error'=>$exception->getMessage()], 401);
        } if($exception instanceof \Illuminate\Auth\Access\AuthorizationException){
            return response()->json(['message'=>'Unauthorized', 'type'=>'auth','error'=>$exception->getMessage()], 403);
        } elseif($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            return response()->json(['message'=>'Url not found', 'type'=>'http', 'error'=>"Url not found"], 404);
        } elseif ($exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException) {
            return response()->json(['message'=>'Entity not found', 'type'=>'entity', 'error'=>$exception->getMessage()], 404);
        }
        \Log::debug($exception);
        return response()->json(['message'=>'Internal Server Error', 'type'=>'server','error'=>$exception->getMessage()], 500);
    }
}
