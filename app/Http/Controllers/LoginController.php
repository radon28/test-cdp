<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\{UserRepositoryInterface, AdminRepositoryInterface};
use App\Http\Requests\{LoginRequest, RegisterAdminRequest, RegisterCustomerRequest};
use Validator;

class LoginController extends Controller
{
    private $userRepository, $adminRepository;
  
    public function __construct(UserRepositoryInterface $userRepository, AdminRepositoryInterface $adminRepository)
    {
        $this->userRepository = $userRepository;
        $this->adminRepository = $adminRepository;
    }

    public function userLogin(LoginRequest $request)
    {
        return $this->userRepository->login($request->validated());
    }

    public function adminLogin(LoginRequest $request)
    {
        return $this->adminRepository->login($request->validated());
    }

    public function createUser(RegisterCustomerRequest $req)
    {
        $user=$this->userRepository->createUser($req->validated());
        return response()->json(['message'=>"User Added", 'data'=>$user], 201);
    }

    public function createAdmin(RegisterAdminRequest $req)
    {
        $user=$this->adminRepository->createUser($req->validated());
        return response()->json(['message'=>"admin Added", 'data'=>$user], 201);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json(['message'=>"User log-out", 'data'=>null], 200);
    }

}
