<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\ProductRepositoryInterface;
use App\Http\Requests\{ProductRequest, StatusChangeRequest};
use App\Models\Product;

class ProductController extends Controller
{
    private $productRepository;
  
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function addProduct(ProductRequest $request)
    {
        $product = $this->productRepository->addProduct($request->validated());
        return response()->json(['message'=>"Product Added", 'data'=>$product], 201);
    }

    public function editProduct(Product $id, ProductRequest $request)
    {
        $product = $this->productRepository->editProduct($id, $request->validated());
        return response()->json(['message'=>"Product updated", 'data'=>$product], 201);
    }

    public function changeStatusOfProduct(Product $id, StatusChangeRequest $request)
    {
        $product = $this->productRepository->editProduct($id, $request->validated());
        return response()->json(['message'=>"Product status updated", 'data'=>$product], 201);
    }

    public function getProducts(Request $request)
    {
        $products = $this->productRepository->all();
        return response()->json(['message'=>"Product list", 'data'=>$products], 201);
    }

    public function getCustomerProducts(Request $request)
    {
        $products = $this->productRepository->getActiveProducts();
        return response()->json(['message'=>"Product list", 'data'=>$products], 201);
    }

    public function deleteProduct(Product $product)
    {
        $product->delete();
        return response()->json(['message'=>"Product deleted", 'data'=>null], 200);
    }
}
