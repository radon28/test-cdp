<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\ValidError;

class LoginRequest extends FormRequest
{
    use ValidError;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $table = $this->route()->getName() == 'adminLogin' ? 'admins' : 'users';
        return [
            'email'=>"required|email|exists:$table,email",
            'password'=>"required|string|max:255"
        ];
    }
}
