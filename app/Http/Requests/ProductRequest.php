<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\ValidError;

class ProductRequest extends FormRequest
{
    use ValidError;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required = $this->route()->getName() == 'add-product' ? 'required' : 'nullable';
        return [
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'price' => ['required', 'regex:/^(?=.*[1-9])\d*(?:\.\d{1,2})?$/s'],
            'discount' => [$required, 'regex:/^(?=.*[1-9])\d*(?:\.\d{1,2})?$/s'],
            'image' => $required.'|image|mimes:jpeg,png,jpg|dimensions:min_width=200,min_height=200|max:2048',
        ];
    }
}
