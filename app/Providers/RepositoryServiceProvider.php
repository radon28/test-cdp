<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind("App\Repository\EloquentRepositoryInterface", "App\Repository\Eloquent\EloquentRepository");
        $this->app->bind("App\Repository\UserRepositoryInterface", "App\Repository\Eloquent\UserRepository");
        $this->app->bind("App\Repository\AdminRepositoryInterface", "App\Repository\Eloquent\AdminRepository");
        $this->app->bind("App\Repository\ProductRepositoryInterface", "App\Repository\Eloquent\ProductRepository");
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
