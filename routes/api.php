<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{LoginController, ProductController};
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('admin/login',[LoginController::class, 'adminLogin'])->name('adminLogin');
Route::post('admin/register',[LoginController::class, 'createAdmin'])->name('adminRegister');
Route::group( ['prefix' => 'admin','middleware' => ['auth:admin-api','scopes:admin'] ],function() {
    Route::post('add-product',[ProductController::class, 'addProduct'])->name('add-product');
    Route::post('edit-product/{id}',[ProductController::class, 'editProduct'])->name('edit-product');
    Route::post('update-product-status/{id}',[ProductController::class, 'changeStatusOfProduct'])->name('update-product-status');
    Route::post('delete-product/{id}',[ProductController::class, 'deleteProduct'])->name('delete-product');
    Route::get('products', [ProductController::class, 'getProducts']);
});
Route::post('user/register',[LoginController::class, 'createUser'])->name('userRegister');
Route::post('user/login',[LoginController::class, 'userLogin'])->name('userLogin');
Route::group( ['prefix' => 'user','middleware' => ['auth:user-api','scopes:user'] ], function() {
    Route::get('products', [ProductController::class, 'getCustomerProducts']);
});
Route::get('logout', [LoginController::class, 'logout'])->middleware('auth:admin-api,user-api');